FROM node:10
WORKDIR /usr/src/naon
COPY . .
RUN npm install --production
RUN npm run build
EXPOSE 3031
CMD [ "npm", "start" ]
