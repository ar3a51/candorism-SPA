export default function(strBase64Image, fnCallback) {
    let maxW=900;
    let maxH=800;
    let img = new Image;
    img.crossOrigin = "anonymous";
    
    img.onload = () => {
        let canvas=document.createElement("canvas");
        let ctx=canvas.getContext("2d");
        
        let iw=img.width;
        let ih=img.height;
        let scale=Math.min((maxW/iw),(maxH/ih));
        let iwScaled=iw*scale;
        let ihScaled=ih*scale;
        canvas.width=iwScaled;
        canvas.height=ihScaled;
        ctx.drawImage(img,0,0,iwScaled,ihScaled);
        fnCallback(canvas.toDataURL());
    }

    img.src = strBase64Image;
}