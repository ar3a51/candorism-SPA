import store from './store/store';

//const loginPage = () => import("./home/home");
const loginPage = () =>import("./login/components/loginPage");
const mainPage = () => import("./main/main.vue");

const newsfeedComponent = () => import(/*webpackChunkName: newsfeed*/ "./main/newsfeed/newsfeed.vue");
const profileComponent = () => import(/*webpackChunkName: profile*/ "./main/profile/profile.vue");
const searchComponent = () => import("./main/search/search");
const postComponent = () => import("./main/post/post");

const isAuthenticated = (to, from, next) => {
    if(!store.getters.isAuthenticated)
        next({name: "login"});
    else
        next();
}

const redirectToMain = (to, from ,next) => {
    if(store.getters.isAuthenticated)
        next({name:"mainPage"})
    else
        next();
}

export const routes = [
    {path: '/', component: loginPage, name: "login", beforeEnter: redirectToMain},
    {
        path: '/main', 
        component: mainPage,
        beforeEnter: isAuthenticated,
        children:[
            {path: '/', component: newsfeedComponent,  name: "mainPage"},
            {path: '/main/profile', component: profileComponent},
            {path: '/main/search', component: searchComponent, props:(route)=>({q: encodeURIComponent(route.query.q)})},
            {path: '/main/post',component:postComponent, props: (route)=>({id: encodeURIComponent(route.query.id)})}
        ],
    },
];